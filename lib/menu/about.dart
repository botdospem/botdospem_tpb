import 'package:flutter/material.dart';

class About extends StatelessWidget {
  const About({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('About'),
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back)),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/rui.jpg"),
              fit: BoxFit.cover,
            ),
          ),
          child: new Card(
              color: Colors.transparent,
              child: new Center(
                child: new Text(
                    "Chat Bot Dospem merupakan sebuah aplikasi konsultasi akademik di bangun oleh Aliansi TPB group. untuk memudahkan mahasiswa melakukan konsultasi akademik.",
                    style: TextStyle(
                      decorationColor: Colors.white,
                      color: Colors.white,
                      fontSize: 16,
                    ),
                    textAlign: TextAlign.center),
              )),
        ),
      ),
    );
  }
}
