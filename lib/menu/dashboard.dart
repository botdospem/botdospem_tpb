import 'package:botdospem/Login/login_screen.dart';
import 'package:botdospem/menu/about.dart';
import 'package:botdospem/menu/profil.dart';
import 'package:botdospem/menu/chat.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("Menu Utama"),
        backgroundColor: Colors.blueAccent,
      ),
      backgroundColor: Colors.blue[100],
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: new Text(
                "1800016004 - Rui Faqri",
                style:
                    new TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
              ),
              accountEmail: new Text("rui1800016004@webmail.uad.ac.id"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(
                    "https://cdn3.iconfinder.com/data/icons/avatars-15/64/_Ninja-2-512.png"),
              ),
            ),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text("Logout"),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: GridView.count(
          crossAxisCount: 2,
          children: <Widget>[
            MyMenu(
              title: "Profil",
              icon: Icons.supervised_user_circle,
              warna: Colors.orange,
              menu: MyApp(),
            ),
            MyMenu(
              title: "Chat",
              icon: Icons.message,
              warna: Colors.green,
              menu: ChatApp(),
            ),
            MyMenu(
              title: "About",
              icon: Icons.info,
              warna: Colors.blue,
              menu: About(),
            )
          ],
        ),
      ),
    );
  }
}

class MyMenu extends StatelessWidget {
  MyMenu({this.title, this.icon, this.warna, this.menu});

  final String title;
  final IconData icon;
  final MaterialColor warna;
  final menu;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8.0),
      child: InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => menu));
        },
        splashColor: Colors.blue,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                icon,
                size: 70.0,
                color: warna,
              ),
              Text(
                title,
                style: new TextStyle(fontSize: 17.0),
              )
            ],
          ),
        ),
      ),
    );
  }
}
