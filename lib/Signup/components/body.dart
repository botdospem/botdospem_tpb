import 'package:flutter/material.dart';
import 'package:botdospem/Login/login_screen.dart';
import 'package:botdospem/Signup/components/background.dart';
import 'package:botdospem/components/already_have_an_account_acheck.dart';
import 'package:botdospem/components/rounded_button.dart';
import 'package:botdospem/components/rounded_input_field.dart';
import 'package:botdospem/components/rounded_password_field.dart';
// import 'package:flutter_svg/svg.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "SIGNUP",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            RoundedInputField(
              hintText: "NIM",
              onChanged: (value) {},
            ),
            RoundedInputField(
              hintText: "Email ",
              onChanged: (value) {},
            ),
            RoundedInputField(
              hintText: "Nama",
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              onChanged: (value) {},
            ),
            RoundedPasswordField(
              onChanged: (value) {},
            ),
            RoundedButton(
              text: "SIGNUP",
              press: () {},
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
