import 'package:flutter/material.dart';
import 'package:botdospem/Login/login_screen.dart';
import 'package:botdospem/Signup/signup_screen.dart';
import 'package:botdospem/Welcome/components/background.dart';
import 'package:botdospem/components/rounded_button.dart';
import 'package:botdospem/constants.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Selamat Datang Di BotDospem",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                decoration: TextDecoration.underline,
                decorationColor: Colors.white,
                color: Colors.black,
                fontSize: 16,
              ),
            ),
            SizedBox(height: size.height * 0.03),
            Image.asset("assets/images/logo.png", height: size.height * 0.45),
            RoundedButton(
              text: "LOGIN",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            RoundedButton(
              text: "SIGN UP",
              color: kPrimaryLightColor,
              textColor: Colors.black,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
